package com.example.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestClass {
    public static List<Integer> testListA = Arrays.asList(2,3,9,2,5,1,3,7,10);
    public static  List<Integer> testListB = Arrays.asList(2,1,3,4,3,10,6,6,1,7,10,10,10);

    public static List<Integer> createListC(List<Integer> listA, List<Integer> listB){
        List<Integer> outList = new ArrayList<>();
        // helper map containing occurrence count for each element of listB
        Map<Integer, Integer> numToOccurrenceMap = new HashMap();
        // helper map containing information about numbers checked for being prime
        Map<Integer, Boolean> numToPrimeMap = new HashMap();

        for(Integer num : listB){
            if(numToOccurrenceMap.containsKey(num)){
                Integer occurrenceCount = numToOccurrenceMap.get(num) + 1;
                numToOccurrenceMap.put(num, occurrenceCount);
            }
            else{
                numToOccurrenceMap.put(num, 1);
            }
        }

        for(Integer num: listA){
            if(numToOccurrenceMap.containsKey(num)){
                Integer occurrenceCount = numToOccurrenceMap.get(num);

                boolean isPrime;
                if(numToPrimeMap.containsKey(num)){
                    isPrime = numToPrimeMap.get(num);
                }
                else {
                    isPrime = testPrime(occurrenceCount);
                    numToPrimeMap.put(num, isPrime);
                }

                if(!isPrime){
                    outList.add(num);
                }
            }
            else {
                outList.add(num);
            }
        }

        return  outList;
    }

    private static boolean testPrime(int num) {
        // case num 1
        if (num == 1)
            return false;
        // case num 2
        if (num == 2)
            return true;

        // checking even numbers
        if ( num % 2 == 0)
            return false;

        // checking only odd numbers from 3 to square root
        for(int i = 3; i * i <= num; i += 2) {
            if(num % i == 0)
                return false;
        }

        return true;
    }
}
